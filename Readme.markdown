# ICS0013 2020 spring semester lab2 project template

## Project structure and files

Structure is inline with [RubyGems basics guide](https://guides.rubygems.org/rubygems-basics/) freewill example and [Learn Ruby the hard way Exercise 46: A Project Skeleton](https://learnrubythehardway.org/book/ex46.html) as far as applicable. Directories contain `.gitkeep` because Git does not track empty directories.

`.gitignore` contains common operating systems and Ruby projects ignored files from [GitHub templates](https://github.com/github/gitignore). It is extended to ignore gitlab-runner builds, RubyMine, VS Code local configuration and other files what shall not belong to repository. See file itself for details. Feel free to extend it.

`lib` directory is structured to modules "style" to enable future expansion.

## Linter configuration

[RuboCop](https://rubocop.readthedocs.io/en/latest/) is used for static code analysis. Project contains `rubocop.yml` with recommended configuration. Feel free to extend it.

## Docker

I have made an image of the application on Docker hub you can use it with 
``` 
docker run -it --name <unameit> --entrypoint=/bin/bash zekena/fix_ois
```
I'm still working on it so it doesn't have the entrypoint flag

## Bundler

It is recommended to use [bundler](https://bundler.io) for projects gems management. 

**Please do not commit local bundler configuration `.bundle/config` to repository.**

Gems shall be installed locally to `vendor/bundle`:

```
$ bundle config set path 'vendor/bundle'
$ bundle install
```

**NB! Please install required Gems first before using rake or any other ruby program**

## Rake

[Rake - Ruby Make](https://ruby.github.io/rake/) is make-like build utility for Ruby.

Rake tasks are set up in `Rakefile` located in project root directory.

```
rake allTests       # Run rubocop and test tasks (default task)
rake clobber_rdoc   # Remove RDoc HTML files
rake db:create      # Create database and schema
rake db:dumpSchema  # Dump database schema
rake rdoc           # Build RDoc HTML files
rake rerdoc         # Rebuild RDoc HTML files
rake rubocop        # Run rubocop
rake test           # Run tests

```

Rake print all tasks usage:

```
$ bundle exec rake -T
```

Rake usage example to run linter and tests:

```
$ rake
Running RuboCop...
Inspecting 7 files
.......

7 files inspected, no offenses detected
Run options: --seed 56121

# Running:

................

Finished in 0.003110s, 5144.6946 runs/s, 7073.9551 assertions/s.

16 runs, 22 assertions, 0 failures, 0 errors, 0 skips

```

## [Active Record](https://rubygems.org/gems/activerecord) database management

Default database configuration is in `config/database.yml`

Temporary "In memory" database is created when running unit tests. See configuration file section `unit_test`.

Development [sqlite](https://www.sqlite.org) database shall be created and also recreated manually by running:

```
$ bundle exec rake db:create
```

Also schema can be dumped from database via rake when you want to make modifications and recreate your own database schema.

## Command line app

### Ad-hoc fix for deprecation warning

It might occur that active model deprecation warning printed out:

```
warning: Using the last argument as keyword parameters is deprecated; maybe ** should be added to the call
```

To avoid this printout you can set environment variable `RUBYOPT` from command line or from IDE:

```
$ export RUBYOPT=-W:no-deprecated
```

### Usage example

To get help:

```
$ bundle exec bin/fix_ois --help
Usage: fix_ois [options]
    -h, --help                       Prints this help
        --db-config FILE             Database configuration file in yml
        --db-environment development  Database configuration file environment in configuration file. 'development' is default
        --import-students FILE       Students list CSV file
        --import-courses FILE        Courses list CSV file
        --print-students Y           Print students
        --print-courses Y            Print courses
        --enrol Y                    Enroll student to course. Requires also --student and --course
        --grade grade date           Grade student. date is optional. Requires also --student and --course
        --course ID                  Course id
        --student name               Student name

```

To import students and courses to database:

```
$ bundle exec bin/fix_ois --db-config config/database.yml --db-config config/database.yml --import-courses data/courses.csv --import-students data/students.csv
Using database configuration config/database.yml environment development
Importing students from data/students.csv:
Alpha Girton added
Bernarda Tafoya added
Julia Caruthers added
Billy Huwe added
Suellen Brazell added
Janet Sicard added
Mitsue Patricio added
Brooke Wanner added
Lacy Fosdick added
Damon Leadbetter added
Season Watley added
Jacqualine Dingus added
Davina Langton added
Zana Stpierre added
Lin Goo added
Quincy Casanova added
Tiffiny Eichenlaub added
Zita Coldiron added
Ardella Garbutt added
Olin Trojacek added

Importing courses from data/courses.csv:
Cooking for beginners 01.01.1970 added
Car engine tuning 01.01.2020 added
Django girls 01.03.2020 added
Django girls 15.03.2020 added
Welding 20.03.2020 added
Welding 2 01.04.2020 added
Theoretical swimming 01.05.2020 added
Practical swimming 01.06.2020 added
Artistic swimming 20.06.2020 added
Winter swimming 01.12.2020 added

Import complete. Exit

```
