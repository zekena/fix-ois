# frozen_string_literal: true

require 'date'
require 'database'

# Grade
class Grade < Database::ApplicationRecord
  def to_s
    "#{number} on #{date}"
  end
end
