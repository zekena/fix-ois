# frozen_string_literal: true

require 'pry'
require 'date'
require 'student'
require 'grade'
require 'database'

# Course class has name and date of the class
class Course < Database::ApplicationRecord
  def to_s
    "#{name} (id: #{id}) starts on #{start_date}"
  end

  def started?
    start_date <= Time.now.to_date
  end

  def enrolled?(student)
    s_id = student.id
    Grade.exists?(student_id: s_id)
  end

  def enrol(student)
    if started?
      # rubocop: disable Layout/LineLength
      raise "Can not enrol #{student.name}. #{name} (id: #{id}) start is in past."
    end

    raise 'User is already enrolled' if enrolled?(student)

    s_id = student.id
    Grade.create(course_id: id, student_id: s_id)
  rescue StandardError => e
    warn e.message
  end

  def enrolled_students
    Grade.joins('INNER JOIN students ON students.id = grades.student_id')
  end

  def grade_student(student, grade_number, date = Time.now.utc.to_date)
    raise "Can not grade not enrolled #{student.name}." unless enrolled?(student)

    s_id = student.id
    ed = Grade.find_by(student_id: s_id)
    Grade.update(ed.id, number: grade_number, date: date)
    # rubocop: enable Layout/LineLength:
  rescue StandardError => e
    warn e.message
  end

  def student_grade(student)
    s_id = student.id
    Grade.where(student_id: s_id).take
  end
end
