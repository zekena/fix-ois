# frozen_string_literal: true

# Logic
module BusinessLogic
  require 'database'
  require 'students'
  require 'courses'
  require 'parse_helper'
  require 'arguments_parser'

  def self.business_logic(options)
    unless options[:db_config_file]
      return abort 'This program can not run without database config file yml.'
    end

    Database.open(options[:db_config_file], options[:db_env] || 'development')

    # Only import data and exit
    if options[:students_file] || options[:courses_file]
      exit import_data(options)
    end

    # Print students and continue
    Students.print_all if options[:print_students]

    # Print courses and continue
    Courses.print_all if options[:print_courses]

    # Enroll student and exit
    exit enrol_student(options) if options[:enrol]

    # Grade student and exit
    exit grade_student(options) if options[:grade]

    exit
  end

  def self.enrol_student(options)
    test_options_student_course(options)
    id = options[:course]
    name = options[:student]
    raise 'Abort' unless (course = Courses.find_course(id))
    raise 'Abort' unless (student = Students.find_student(name))

    puts "Enrolling #{student.name} to #{course.name}"
    raise 'Abort' unless course.enrol(student)

    puts 'Done!'
    # Return true for exit
    true
  rescue StandardError => e
    warn e.message
    abort
  end

  def self.grade_student(options)
    test_options_student_course(options)
    id = options[:course].to_i
    name = options[:student]
    array = options[:grade]
    grade = array.slice!(0).to_i

    date = if array.empty?
             Time.now.utc.to_date
           else
             ParseHelper.parse_date(array.to_s.strip)
           end

    raise 'Abort' unless (course = Courses.find_course(id))
    raise 'Abort' unless (student = Students.find_student(name))

    puts "Grading #{student.name} in #{course.name} with #{grade}"
    raise 'Abort' unless course.grade_student(student, grade, date)

    puts 'Done!'
    # Return true for exit
    true
  rescue StandardError => e
    warn e.message
    abort
  end

  def self.import_data(options)
    Students.import_from_csv(options[:students_file]) if options[:students_file]
    Courses.import_from_csv(options[:courses_file]) if options[:courses_file]
    puts 'Import complete. Exit', ''
    # Return true for exit
    true
  end

  def self.test_options_student_course(options)
    raise '--course argument shall be provided' unless options[:course]
    raise '--student argument shall be provided' unless options[:student]
  rescue StandardError => e
    warn e.message
    abort
  end
end
