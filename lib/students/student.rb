# frozen_string_literal: true

require 'database'

# student name
class Student < Database::ApplicationRecord
  def to_s
    "#{name} (id: #{id})"
  end
end
