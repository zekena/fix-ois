# frozen_string_literal: true

# Parse yaml format database configuration
module Database
  require 'sqlite3'
  require 'active_record'
  require 'file_helper'

  # rubocop: disable Metrics/MethodLength
  # rubocop: disable Metrics/AbcSize
  def self.open(config_yml, db_env)
    puts "Using database configuration #{config_yml} environment #{db_env}"
    file = FileHelper.open_read(config_yml)
    db_config = parse_config_file(file)
    unless db_env == 'unit_test'
      if db_config[db_env]['adapter'] == 'sqlite3'
        unless File.exist?(db_config[db_env]['database'])
          raise "Database file #{db_config[db_env]['database']} does not exist"
        end
      end
    end

    ActiveRecord::Base.establish_connection(db_config[db_env])
  rescue StandardError => e
    warn e.message
    abort
  end
  # rubocop: enable Metrics/MethodLength
  # rubocop: enable Metrics/AbcSize

  # ActiveRecord
  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true
  end
  private_class_method def self.parse_config_file(file)
    YAML.safe_load(file)
  rescue StandardError => e
    warn "#{e.message} when parsing #{File.basename(file)}"
    abort
  end
end
