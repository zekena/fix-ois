FROM ruby:2.7
RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./

COPY . .

RUN bundle install \
    && apt-get update \
    && bundle exec rake db:create

CMD ["bundle", "exec", "bin/fix_ois", "-h"]
ENTRYPOINT ["/bin/bash"]