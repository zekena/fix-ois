#!/bin/bash

bundle exec bin/fix_ois -h;
bundle exec bin/fix_ois --db-config config/database.yml --import-students data/students.csv --import-courses data/courses.csv;
bundle exec bin/fix_ois --db-config config/database.yml --import-students data/students.csv --import-courses data/courses.csv;
bundle exec bin/fix_ois --db-config config/database.yml --db-environment development --print-students Y --print-courses Y;
bundle exec bin/fix_ois --db-config config/database.yml --db-environment development --enrol Y --course 7 --student "Lin Goo";
bundle exec bin/fix_ois --db-config config/database.yml --db-environment development --course 7 --student "Lin Goo" --grade 5 29.02.2020;
